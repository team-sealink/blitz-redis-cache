<?php

use sealink\blitzredis\RedisQuickCache;
use Kahlan\Plugin\Double;

describe("RedisQuickCache", function() {
  beforeEach(function() {
    $this->quickCache = new RedisQuickCache();
    $_SERVER['HTTP_HOST'] = 'https://test.com';
    $_SERVER['REQUEST_URI'] = '/test';
    $_SERVER['QUERY_STRING'] = '';
  });

  describe("#exists", function() {
    describe("When the connection is not set", function() {
      it("should return false", function() {
        expect($this->quickCache->exists())->toBe(false);
      });
    });

    describe("When the connection is set", function() {
      beforeEach(function() {
        $this->quickCache->_redis = Double::instance();
      });

      describe("When the key is set", function() {
        it("should return true", function() {
          allow($this->quickCache->_redis)->toReceive('exists')->andReturn(1);
          expect($this->quickCache->exists())->toBe(true);
        });

        describe("when the preview query param is set", function() {
          beforeEach(function() {
            $_SERVER['QUERY_STRING'] = 'x-craft-live-preview=lol';
          });

          it("should return false", function() {
            expect($this->quickCache->exists())->toBe(false);
          });
        });
      });

      describe("When the key is not set", function() {
        it("should return false", function() {
          allow($this->quickCache->_redis)->toReceive('exists')->andReturn(0);
          expect($this->quickCache->exists())->toBe(false);
        });
      });
    });
  });

  describe("#get", function() {
    describe("When the connection is not set", function() {
      it("should return a blank string", function() {
        expect($this->quickCache->get())->toBe('');
      });
    });

    describe("When the connection is set", function() {
      beforeEach(function() {
        $this->quickCache->_redis = Double::instance();
      });

      describe("When the key is set", function() {
        it("should return the cache entry with metadata", function() {
          allow($this->quickCache->_redis)->toReceive('get')->andReturn('a:1:{i:0;s:3:"lol";}');
          expect($this->quickCache->get())->toMatch('/^lol<!-- .* -->$/');
        });
      });

      describe("When the key is not set", function() {
        it("should return a blank string", function() {
          allow($this->quickCache->_redis)->toReceive('get')->andReturn(false);
          expect($this->quickCache->get())->toBe('');
        });
      });
    });
  });
});
