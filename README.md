# Craft Blitz Redis

Yii Redis cache support for Craft CMS and blitz. The purpose of this package is to provide
an interface that can be used to retrieve the HTML page cache before the Craft CMS app engine
has been loaded.

## Usage

Add the following to `index.php` after composer initialisation and before the Craft
initialisation.

```
use sealink\blitzredis\RedisQuickCache;

if (!getenv('DISABLE_QUICKCACHE')) {
  // Return the cached page if it exists
  $quickCache = new RedisQuickCache();
  $quickCache->open();
  if ($quickCache->exists()) {
    echo $quickCache->get();
    $quickCache->close();
    return;
  }
  $quickCache->close();
}
```
