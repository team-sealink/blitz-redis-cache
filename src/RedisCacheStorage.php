<?php
namespace sealink\blitzredis;

use Craft;
use craft\helpers\FileHelper;
use putyourlightson\blitz\drivers\storage\BaseCacheStorage;
use putyourlightson\blitz\models\SiteUriModel;

/**
 *
 * @property mixed $settingsHtml
 */
class RedisCacheStorage extends BaseCacheStorage
{
    // Constants
    // =========================================================================

    /**
     * @const string
     */
    const KEY_PREFIX = 'blitz';

    // Properties
    // =========================================================================

    /**
     * @var string
     */
    public $cacheComponent = 'cache';

    /**
     * @var object|null
     */
    private $_cache;

    // Static
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function displayName(): string
    {
        return Craft::t('blitz', 'Redis Cache Storage');
    }

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->_cache = Craft::$app->get($this->cacheComponent, false);
    }

    /**
     * @inheritdoc
     */
    public function get(SiteUriModel $siteUri): string
    {
        if ($this->_cache === null) {
            return '';
        }

        $value = $this->_cache->get($this->_cacheKey($siteUri));

        if ($value === false) {
            return '';
        }

        return $value;
    }

    /**
     * @inheritdoc
     */
    public function save(string $value, SiteUriModel $siteUri)
    {
        if ($this->_cache === null) {
            return;
        }

        $this->_cache->set($this->_cacheKey($siteUri), $value);
    }

    /**
     * @inheritdoc
     */
    public function deleteUris(array $siteUris)
    {
        if ($this->_cache === null) {
            return;
        }

        foreach ($siteUris as $siteUri) {
            $this->_cache->delete($this->_cacheKey($siteUri));
        }
    }

    /**
     * @inheritdoc
     */
    public function deleteAll()
    {
        if ($this->_cache === null) {
            return;
        }

        $this->_cache->flush();
    }

    /**
     * Returns a normalised cache key for the current URI.
     *
     * @param SiteUriModel $siteUri
     *
     * @return string
     */
    private function _cacheKey(SiteUriModel $siteUri): string
    {
        $siteHost = $this->_getSiteHost($siteUri->siteId);

        if ($siteHost == '') {
            return '';
        }

        // Create normalized key from the site host and uri
        return self::KEY_PREFIX.':'.$siteHost.$siteUri->uri;
    }

    /**
     * Returns site host for the provided site ID.
     *
     * @param int $siteId
     *
     * @return string
     */
    private function _getSiteHost(int $siteId): string
    {
        // Get the site host and path from the site's base URL
        $site = Craft::$app->getSites()->getSiteById($siteId);
        $siteUrl = Craft::getAlias($site->getBaseUrl());

        // Get the URL host and port without the protocol
        return preg_replace('/^(http|https):\/\//i', '', $siteUrl);
    }
}
