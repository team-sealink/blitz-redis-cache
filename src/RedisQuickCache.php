<?php
namespace sealink\blitzredis;

use Redis;

class RedisQuickCache
{
  // Properties
  // =========================================================================

  /**
   * @var object|null
   */
  public $_redis;

  // Public Methods
  // =========================================================================

  /**
   * Opens the Redis connection.
   *
   * @param int $siteId
   *
   * @return string
   */
  public function open()
  {
    $this->_redis = new Redis();
    $this->_redis->connect(getenv('REDIS_HOST'), getenv('REDIS_PORT'), 1.0);
    $this->_redis->select(getenv('REDIS_DB'));
  }

  /**
   * Closes the Redis connection.
   *
   * @param int $siteId
   *
   * @return string
   */
  public function close()
  {
    if ($this->_redis === null) {
      return;
    }

    $this->_redis->close();
    $this->_redis = null;
  }

  /**
   * Returns the status of the page cache for the current URI.
   *
   * @return bool
   */
  public function exists(): bool
  {
    if ($this->_redis === null) {
      return false;
    }

    if ($this->_isPreview()) {
      return false;
    }

    return $this->_redis->exists($this->_cacheKey()) === 1;
  }

  /**
   * Returns the cached page HTML for the current URI.
   *
   * @return string
   */
  public function get(): string
  {
    if ($this->_redis === null) {
      return '';
    }

    $value = $this->_redis->get($this->_cacheKey());

    if ($value === false) {
      return '';
    }

    return unserialize($value)[0].$this->_meta();
  }

  /**
   * Returns a normalised cache key for the current URI.
   *
   * @return string
   */
  public function _cacheKey(): string
  {
    // Get the URL host and port without the protocol
    $host = preg_replace('/^(http|https):\/\//i', '', $_SERVER['HTTP_HOST']);
    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $hash = md5('blitz:'.$host.$path);
    return 'stg_website_platform'.$hash;
  }

  /**
   * Returns the cache debug metadata.
   *
   * @return string
   */
  public function _meta(): string
  {
    return '<!-- Served by RedisQuickCache on '.date('c').' -->';
  }

  /**
   * Checks if the current page is a Craft Preview URL
   *
   * @return bool
   */
  public function _isPreview(): bool
  {
    $query = array();
    parse_str($_SERVER['QUERY_STRING'], $query);

    return isset($query['x-craft-live-preview']);
  }
}
